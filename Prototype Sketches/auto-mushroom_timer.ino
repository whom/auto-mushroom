long hours_1_millis = 3600000; //number of millis in an hour
long hours_12_millis = 43200000; //number of millis in 12 hours
long hours_4_millis = 14400000; //number of millis in 4 hours
long hours_20_millis = 72000000; //number of millis in 20 hours
long preWaterTime = 0;
long lightOnTime = 0;
long lightOffTime = 0;
long currTime = 0;
bool lightOn = false;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

}

void loop() {
  // put your main code here, to run repeatedly:
  currTime = millis();
  if( (currTime - preWaterTime) >= hours_1_millis)
  {
    preWaterTime = currTime;
    //water mushroom
    Serial.print("watering mushroom");
  }
  if(lightOn)
  {
    if( (currTime-lightOnTime) >= hours_4_millis)
    {
      lightOffTime = currTime;
      lightOn = false;
      //turn off light
      Serial.print("light off");
    }
  }
  else
  {
    if( (currTime-lightOffTime) >= hours_20_millis)
    {
      lightOnTime = currTime;
      lightOn = true;
      //turn light on
      Serial.print("light on");
    }
  }
  

}
