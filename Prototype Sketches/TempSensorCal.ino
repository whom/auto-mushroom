/*  Soil Mositure & Relay

*/

#define ON   0
#define OFF  1

// Relay Pins
int IN1 = 12;  // pump
int IN2 = 13;  // lamp

// Moisure Pins
int val = 0; //value for storing moisture value 
int soilPin = A0;//Declare a variable for the soil moisture sensor 
int soilPower = 7;//Variable for Soil moisture Power

//Rather than powering the sensor through the 3.3V or 5V pins, 
//we'll use a digital pin to power the sensor. This will 
//prevent corrosion of the sensor as it sits in the soil. 

void setup() {
  Serial.begin(9600);   // open serial over USB
  relay_init();       // initialize relay
  pinMode(soilPower, OUTPUT);//Set D7 as an OUTPUT
  digitalWrite(soilPower, LOW);//Set to LOW so no power is flowing through the sensor
}

void loop() {
  Serial.print("Soil Moisture = ");    
  //get soil moisture value from the function below and print it
  Serial.println(readSoil());

  //This 1 second timefrme is used so you can test the sensor and see it change in real-time.
  //For in-plant applications, you will want to take readings much less frequently.
  delay(1000);//take a reading every second

  relay_SetStatus(ON, ON);//turn on RELAY_1
  delay(2000);//delay 2s
  relay_SetStatus(OFF, OFF);//turn on RELAY_2
  delay(10000);//delay 2s
}

//This is a function used to get the soil moisture content
int readSoil() {
  digitalWrite(soilPower, HIGH);//turn D7 "On"
  delay(10);//wait 10 milliseconds 
  val = analogRead(soilPin);//Read the SIG value form sensor 
  digitalWrite(soilPower, LOW);//turn D7 "Off"
  return val;//send current moisture value
}

// Set up relay
void relay_init(void) {
  //set all the relays OUTPUT
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  relay_SetStatus(OFF, ON); //turn off all the relay
}

//set the status of relays
void relay_SetStatus( unsigned char status_1,  unsigned char status_2) {
  digitalWrite(IN1, status_1);
  digitalWrite(IN2, status_2);
}