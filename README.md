# auto-mushroom

Automated growth of oyster mushrooms with arduino.
This repository describes a relatively simple machine that controls the light and watering of inoculated substrate.
The code for our machine is included.

## Contributing 
We encourage anyone to lodge issues or pull requests.
We are not professional mycologists. We would love to hear about your research and suggestions on the proper amount of water and light to adminsiter to oyster mushrooms.

## Parts List
####	Misting Subsystem:
		-Mist nozzle
		-AC Brushless Pump
		-Distilled water reservoir (2 liter bottle)
		-Soil Moisture Sensor 
####	Light Subsystem:
		- LED bulb
		- AC lightbulb socket
####	Arduino: We used a mega 2560
####	Dual AC Relay
####	Mushroom Log
####	Fan
####	LED display

## Contact Us
Aidan Hahn, aidan@aidanis.online
Carla Colaianne, carla.colaianne@gmail.com
Terry Cox, coxte@protonmail.com
