/*
 * AUTO MUSHROOM
 * Aidan Hahn, Terry Cox, Carla Colaianne
 *
 * This code is meant to run on an arduino and optimize the growth of oyster mushrooms.
 * Current settings indicate 1 seconds of water sprizing per hour, and 4 hours of light per 12 hours.
 * We highly encourage discussion about the optimal levels of growth directly in our repository.
 * There is also a parts list in our repository.
 * For any questions please email aidan@aidanis.online
 */

#include <LiquidCrystal.h>

#define ON   0
#define OFF  1

#define DEBUG(X) debug? Serial.println(X) : 0;

#define sec_millis          1000
#define tensec_millis      10000
#define hours_1_millis   3600000
#define hours_4_millis  14400000
#define hours_12_millis 43200000
#define hours_20_millis 72000000

// For LCD screen
LiquidCrystal lcd(24, 26, 28, 30, 32, 34);

bool debug = true;

int pumpOnTime = 0;
int lightOnTime = 0;
unsigned long timeSincePump = debug? hours_1_millis : 0;
unsigned long timeSinceLight = debug? hours_12_millis : 0;

unsigned long lastTime = 0;

int moisture = 0;
bool lightOn = false;
bool pumpOn = false;

int soilReadPin = A0;
int soilPowerPin = 11; //               off by default
int pumpRelayPin = 12; //               off by default
int lightRelayPin = 13; //              on by default

void setup() {
  pinMode(soilReadPin,   INPUT );
  pinMode(soilPowerPin,  OUTPUT);
  pinMode(pumpRelayPin,  OUTPUT);
  pinMode(lightRelayPin, OUTPUT);

  digitalWrite(soilPowerPin, LOW );    // off
  digitalWrite(pumpRelayPin, HIGH);    // off
  digitalWrite(lightRelayPin,LOW );    // off

  lcd.begin(16, 2);
  lcd.print("Moisture in Soil");
  Serial.begin(9600);
}

void readSoil(){
    digitalWrite(soilPowerPin, HIGH);
    delay(10); 
    // our analog pin was reading 390 as a baseline voltage.
    moisture = analogRead(soilReadPin) - 390; // Read the SIG value form sensor 
    digitalWrite(soilPowerPin, LOW);
}

void loop() {
  unsigned long t = millis();
  long timeDelta = t - lastTime; //     milliseconds since last read
  lastTime = t;
  
  DEBUG("Current TD")
  DEBUG(timeDelta)
  DEBUG("Pump Uptime")
  DEBUG(pumpOnTime)
  DEBUG("Light Uptime")
  DEBUG(lightOnTime)
  DEBUG("Time since last water")
  DEBUG(timeSincePump)
  DEBUG("Time since last light")
  DEBUG(timeSinceLight)
  DEBUG("Soil moisture")
  DEBUG(moisture)
  DEBUG("--------------------")
  
  
  // turn off light?
  if(lightOn){
    
    // make sure light only stays on for 4 hours
    if(lightOnTime > hours_4_millis){
      // turn off light, reset timeSinceLight
      DEBUG("[+] Turning off light")
      digitalWrite(lightRelayPin, LOW);
      timeSinceLight = 0;
    
    } else {
      lightOnTime += timeDelta;  
    }
    
  } else {
    
    // 4 hours of light every 12 hours
    if(timeSinceLight > hours_12_millis - hours_4_millis){
      // reset lightOnTime, tirn on light
      DEBUG("[+] Turning on light")
      lightOnTime = 0;
      digitalWrite(lightRelayPin, HIGH);
      lightOn = true;
        
    } else {
      timeSinceLight += timeDelta;
    }
  }

  // turn off pump?
  if(pumpOn){
    
    // make sure pump only stays on for 5 seconds
    if(pumpOnTime > sec_millis){
      DEBUG("[+] Turning off pump")
      // turn off pump, reset timeSincePump  
      digitalWrite(pumpRelayPin, HIGH);
      timeSincePump = 0;
    
    } else {
      pumpOnTime += timeDelta;
    }
    
  } else {
    // 5 seconds of pump every hour 
    if(timeSincePump > hours_1_millis - sec_millis){
      // Read moisure & print results
      readSoil();
      lcd.setCursor(0, 1);
      lcd.print(moisture/910.00);
 
      // reset pumpOnTime, turn on pump
      DEBUG("[+] Turning on pump")
      pumpOnTime = 0;
      digitalWrite(pumpRelayPin, LOW);  
      pumpOn = true;
    
    } else {
      timeSincePump += timeDelta;
    }
  }
}
